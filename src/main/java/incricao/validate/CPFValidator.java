package incricao.validate;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator(value="cpfValidator")
public class CPFValidator implements Validator {
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String cpf = value.toString();
		if (!Pattern.matches("\\d{3}\\.\\d{3}\\.\\d{3}\\-\\d{2}|\\d{11}|\\d{9}\\-\\d{2}", cpf)) {
            FacesMessage msg = new FacesMessage(String.format("CPF '%s' inválido", cpf));
            throw new ValidatorException(msg);
        }
	}

}
