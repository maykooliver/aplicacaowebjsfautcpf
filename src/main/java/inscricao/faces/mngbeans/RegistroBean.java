package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.ListDataModel;

import utfpr.faces.support.PageBean;

@ManagedBean
@ApplicationScoped
public class RegistroBean extends PageBean {
	private List<Candidato> candidatos = new ArrayList<>();
	private ListDataModel<Candidato> candidatosModel = new ListDataModel<>(candidatos);
	
	public void adicionarCandidato(Candidato candidato) {
		this.candidatos.add(candidato);
	}
	
	public List<Candidato> getCandidatos() {
		return this.candidatos;
	}
	
	public ListDataModel<Candidato> getCandidatosModel() {
		return this.candidatosModel;
	}
	
	public String visualizarCandidato() {
		Candidato candidato = this.candidatosModel.getRowData();
		InscricaoBean inscricao = (InscricaoBean) this.getBean("inscricaoBean");
		inscricao.setCandidato(candidato);
		return "confirma";
	}
	
	public String excluirCandidato() {
		Candidato candidato = this.candidatosModel.getRowData();
		this.candidatos.remove(candidato);
		return "candidatos";
	}
}
